import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// 类过于繁琐，可以直接写成函数
// class Square extends React.Component {
//   render() {
//     return (
//       <button className="square"
//         onClick={() => this.props.onClick()}
//       >
//         {this.props.value}
//       </button>
//     );
//   }
// }

// function Square(props) {
//   return (
//     <button className="square" onClick={props.onClick}>{props.value}</button>
//   )
// }

class Sort extends React.Component {
  render() {
    return (
      <button onClick={() => { this.props.onClick() }}>sort</button>
    )
  }
}

class WinnerLine extends React.Component {
  render() {
    console.log(this.props.value);
    let typeClass = 'none';
    switch (this.props.value) {
      case 0:
        typeClass = 'row-top';
        break;
      case 1:
        typeClass = 'row-middle';
        break;
      case 2:
        typeClass = 'row-bottom';
        break;
      case 3:
        typeClass = 'column-top';
        break;
      case 4:
        typeClass = 'column-middle';
        break;
      case 5:
        typeClass = 'column-bottom';
        break;
      case 6:
        typeClass = 'left-to-right';
        break;
      case 7:
        typeClass = 'right-to-left';
        break;
      default:
        typeClass = 'none';
        break;
    }
    console.log(typeClass);
    return (
      <div className={`line-through ${typeClass}`}></div>
    )
  }
}

// React 中，有一个命名规范，通常会将代表事件的监听 prop 命名为 on[Event]，将处理事件的监听方法命名为 handle[Event] 这样的格式。
class Board extends React.Component {
  render() {
    const arr = [0, 1, 2];
    let count = -1;
    const block = arr.map(i =>
      <div key={i.toString()} className="board-row">
        {
          arr.map(() => {
            let index = ++count;
            return <button className="square" key={index}
              onClick={() => this.props.onClick(index)}>{this.props.squares[index]}
            </button>
          })
        }
      </div>
    )
    return (
      <div>{block}</div>
    )
  }
}

class Game extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      history: [{
        squares: Array(9).fill(null),
        index: NaN,
      }],
      xIsNext: true,
      stepNumber: 0,
      row: 0,
      column: 0,
    }
  }
  handleClick(i) {
    // 直接改变法，没有创建一个副本来把保存，无法回退
    // const squares = this.state.squares;
    // squares[i] = 'X';
    // 副本法,可回溯
    // const squares = [...this.state.squares];
    // 这行的意思是，当胜出时，点击就无效了，当squares[i]有值时，就是点击过一次时，也不赋值
    const history = this.state.history.slice(0, this.state.stepNumber + 1);
    const current = history[history.length - 1];
    // 这里的squares用一个副本保存了
    const squares = [...current.squares];
    if (calculateWinner(squares)[0] || squares[i]) {
      return;
    }
    // 根据xIsNext对这个副本就行赋值
    squares[i] = this.state.xIsNext ? 'X' : 'O';
    this.setState({
      history: history.concat([{
        squares: squares,
        index: i,
      }]),
      stepNumber: history.length,
      xIsNext: !this.state.xIsNext,
      index: i,
      sort: false,
    })
  }
  jumpTo(step) {
    const history = this.state.history.slice(0, step + 1);
    this.setState({
      history: history,
      stepNumber: step,
      xIsNext: (step % 2) === 0,
    })
  }
  render() {
    const history = this.state.history;
    const current = history[this.state.stepNumber];
    const [winner, type] = calculateWinner(current.squares);
    let desc;

    const moves = history.map((step, move) => {
      const index = step.index;
      const row = Math.floor(index / 3) + 1;
      const column = index - 3 * (row - 1) + 1;
      if (!this.state.sort) {
        desc = move ?
          `Go to move #' ${move} & X: ${row} & Y: ${column}` : 'Go to game start';
      } else {
        desc = move === history.length - 1 ?
          'Go to game start' : `Go to move #' ${move} & X: ${row} & Y: ${column}`;
      }
      if (this.state.stepNumber === 9 && !winner) {
        desc = "平局";
      }
      return (
        <li className='history' key={move}>
          <button className={move === history.length - 1 ? 'button primary' : 'button error'}
            onClick={() => this.jumpTo(move)}>{desc}</button>
        </li>
      )
    })

    let status;
    if (winner) {
      status = `Winner:${winner}`;
    } else {
      status = `Next player: ${this.state.xIsNext ? 'X' : 'O'}`;
    }
    return (
      <div className="game">
        <div className="game-board">
          <Board squares={current.squares}
            onClick={(i) => this.handleClick(i)}>
          </Board>
          <WinnerLine value={type}></WinnerLine>
        </div>

        <div className="game-info">
          <Sort onClick={() => {
            this.setState({
              history: this.state.history.reverse(),
              sort: !this.state.sort,
            })
          }}></Sort>
          <div>{status}</div>
          <ol>{moves}</ol>
        </div>
      </div>
    );
  }
}

// ========================================

ReactDOM.render(
  <Game />,
  document.getElementById('root')
);

function calculateWinner(squares) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8], // 横排
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8], // 竖排
    [0, 4, 8], // 右斜排
    [2, 4, 6], // 左斜排
  ]
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    // debugger;
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return [squares[a], i];
    }
  }
  return [];
}
